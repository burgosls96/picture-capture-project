import { Component, OnInit } from '@angular/core';
import {Subject, Observable} from 'rxjs';
import {WebcamImage, WebcamInitError, WebcamUtil} from 'ngx-webcam';
import { ImageServiceService } from './services/image-service.service';
import { ResponseInterface } from './interfaces/ResponseInterface';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

    // emotion selected
    public selectedValue: string;
    // emotions array
    public emotionsArray: string[];

    // toggle webcam on/off
    public showWebcam = true;

    public acceptPolicy = false;

    public multipleWebcamsAvailable = false;

    public videoOptions: MediaTrackConstraints = {
      // width: {ideal: 1024},
      // height: {ideal: 576}
    };

    public errors: WebcamInitError[] = [];
    // latest snapshot
    public webcamImage: WebcamImage = null;
    // webcam snapshot trigger
    private trigger: Subject<void> = new Subject<void>();

    public constructor(private imageService: ImageServiceService){

    }
    public ngOnInit(): void {
      WebcamUtil.getAvailableVideoInputs()
        .then((mediaDevices: MediaDeviceInfo[]) => {
          this.multipleWebcamsAvailable = mediaDevices && mediaDevices.length > 1;
        });

      this.imageService.getEmotions()
      .subscribe((data: ResponseInterface) => {
        this.emotionsArray = data.data;
      },
      err => {
        console.log(err.message);
      });

    }
    public triggerSnapshot(): void {
      this.trigger.next();
    }

    public validatePolicy(event): void {
      this.acceptPolicy = event.target.checked;
    }
    public toggleWebcam(): void {
      this.showWebcam = !this.showWebcam;
    }
    public handleInitError(error: WebcamInitError): void {
      this.errors.push(error);
    }
    public handleImage(webcamImage: WebcamImage): void {
      this.webcamImage = webcamImage;
      this.showWebcam = false;
    }

    public sendPicture(){
      if (this.selectedValue && this.webcamImage){
        this.imageService.saveImage(this.selectedValue, this.webcamImage.imageAsBase64)
        .subscribe(res => {
          if (res.status === 204) {
              alert('¡Gracias por tu colaboración!');
              this.selectedValue = null;
              this.webcamImage = null;
              this.acceptPolicy = false;
              this.showWebcam = true;
          } else {
            alert('Ups! Parece que ha ocurrido un error enviando tus datos. Porfavor, inténtalo de nuevo más tarde.');
          }
        });
      } else {
        alert('Porfavor, selecciona una emoción para etiquetar tu imagen');
      }
    }
    public get triggerObservable(): Observable<void> {
      return this.trigger.asObservable();
    }
}
