import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ImageServiceService {

  constructor(private http: HttpClient) { }

  getEmotions(): Observable<Object> {
    return this.http.get(environment.emotions_endpoints);
  }

  saveImage(emotion: string, base64Image: string): Observable<HttpResponse<Object>> {
    return this.http.post(environment.images_endpoints, {'emotion': emotion, 'image': base64Image}, {observe: 'response'});
  }

}
