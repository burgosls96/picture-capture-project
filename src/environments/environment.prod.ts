export const environment = {
  production: true,
  emotions_endpoints: "api/v1/emotions",
  images_endpoints: "api/v1/images",
};
